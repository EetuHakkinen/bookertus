const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.PORT || 8000;

app.use(express.static('build'));

app.listen(port, () => {
    console.log(`Listening port ${port}`)
}); 